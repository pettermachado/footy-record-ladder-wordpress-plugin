<?php
/*
	Plugin Name: Footy Record Ladder
	Plugin URI: http://www.pettereek.se/footy-record-ladder/
	Description: This plugin heads over to thefootyrecord.net and fetches the ladder form the given competition. Allows to select what columns to show.
	Version: 0.0.2
	Author: Petter Eek
	Author URI: http://www.pettereek.se
	License: GPL2
 
	Copyright 2010 - current  Petter Eek  (email : hello@pettereek.se)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
*/

/**
 * FootyRecord Ladder
 */
class fr_ladder_widget extends WP_Widget {

	function fr_ladder_widget() {
		$widget_ops = array('classname' => 'fr_ladder_widget', 'description' => __( "Display a Footy Record Ladder") );
		$this->WP_Widget('fr_ladder_widget', __('FootyRecord Ladder'), $widget_ops);
	}

	function widget( $args, $instance ) {
		extract($args);
		$competitionId = $instance['competitionId'];
		
		if (!$competitionId) :
			echo 'No competition ID supplied.';
			return; // If there's no competition selected, there's no need to go any further.
		endif;
		
		$title = apply_filters('widget_title', $instance['title'], $instance, $this->id_base);
		
		// Include the HTML DOM parser
		include('simple_html_dom.php');
		
		$url = 'http://www.thefootyrecord.net/index.php?content1=comp&c='.$competitionId.'&taboption=4';
		
		$error = false;
		$html = @file_get_html($url);
		
		if ($html != null) {
			$table = $html->find('#rstable table'); // Find the table...
			$table = $table[0]; // and store it in a handy form
			
			$table->cellspacing = '0';
			$table->cellpadding = '0';
			
			if (method_exists($table, 'find')) :
				$table->find('tr', 1)->class .= ' leader'; // Indicate who's on top ;)
				
				foreach( $table->find('img') as $img ) : 
					$img->src = "http://www.thefootyrecord.net/" . $img->src; // Change the image-urls
				endforeach;
		
				foreach( $table->find('tr') as $e ) : // Display the selected columns
					if (!$instance['col_image'])
						$e->children(0)->outertext = '';
					if (!$instance['col_name'])
						$e->children(1)->outertext = '';
					$e->children(1)->class .= ' team';
					if (!$instance['col_played'])
						$e->children(2)->outertext = '';
					if (!$instance['col_win'])
						$e->children(3)->outertext = '';
					if (!$instance['col_loss'])
						$e->children(4)->outertext = '';
					if (!$instance['col_draw'])
						$e->children(5)->outertext = '';
					if (!$instance['col_forfeits'])
						$e->children(6)->outertext = '';
					if (!$instance['col_points'])
						$e->children(7)->outertext = '';
					if (!$instance['col_ptfor'])
						$e->children(8)->outertext = '';
					if (!$instance['col_ptagst'])
						$e->children(9)->outertext = '';
					if (!$instance['col_pct'])
						$e->children(10)->outertext = '';
				endforeach;
				
				// Change P.C. to %
				$table = str_replace('P.C.', '%', $table);
				
				else : // Function 'find' does not exist on $table, seems to be the most common error
					$error = true;
					$error_msg = "Error retrieving ladder-data from TFR.";
			endif;
		}
		// print the actual widget
		echo $before_widget;
		if ( $title ) :
			echo $before_title . $title . $after_title; 
		endif; ?>
			<div id="tfr_content">
				<?php if (!$error) : ?>
				<?php echo utf8_encode( $table ); ?>
				<?php else : ?>
				<p class="trf-error"><?php echo $error_msg; ?> </p>
				<?php endif; ?>
				<p class="tfr-credit">Data from <a href="<?php echo $url; ?>">The Footy Record</a></p>
			</div><!-- #tfr_content --> <?php
		echo $after_widget;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '',	'competitionId' => '', 'col_image' => '', 'col_name' => '', 'col_played' => '', 'col_win' => '', 'col_loss' => '', 'col_draw' => '', 'col_forfeits' => '', 'col_points' => '', 'col_ptfor' => '', 'col_ptagst' => '', 'col_pct' => '' ));
		
		$title 			= $instance['title'];
		$competitionId 	= $instance['competitionId'];
		$col_image 		= $instance['col_image'] ? 'checked="checked"' : '';
		$col_name 		= $instance['col_name'] ? 'checked="checked"' : '';
		$col_played 	= $instance['col_played'] ? 'checked="checked"' : '';
		$col_win 		= $instance['col_win'] ? 'checked="checked"' : '';
		$col_loss 		= $instance['col_loss'] ? 'checked="checked"' : '';
		$col_draw 		= $instance['col_draw'] ? 'checked="checked"' : '';
		$col_forfeits	= $instance['col_forfeits'] ? 'checked="checked"' : '';
		$col_points 	= $instance['col_points'] ? 'checked="checked"' : '';
		$col_ptfor 		= $instance['col_ptfor'] ? 'checked="checked"' : '';
		$col_ptagst 	= $instance['col_ptagst'] ? 'checked="checked"' : '';
		$col_pct 		= $instance['col_pct'] ? 'checked="checked"' : '';
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id('competitionId'); ?>"><?php _e('Competition Id:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('competitionId'); ?>" name="<?php echo $this->get_field_name('competitionId'); ?>" type="text" value="<?php echo $competitionId; ?>" /></label></p>
		<p>Columns:</p>
		<p>
			<input class="checkbox" type="checkbox" <?php echo $col_image; ?> id="<?php echo $this->get_field_id('col_image'); ?>" name="<?php echo $this->get_field_name('col_image'); ?>" /> <label for="<?php echo $this->get_field_id('col_image'); ?>"><?php _e('Image'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_name; ?> id="<?php echo $this->get_field_id('col_name'); ?>" name="<?php echo $this->get_field_name('col_name'); ?>" /> <label for="<?php echo $this->get_field_id('col_name'); ?>"><?php _e('Name'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_played; ?> id="<?php echo $this->get_field_id('col_played'); ?>" name="<?php echo $this->get_field_name('col_played'); ?>" /> <label for="<?php echo $this->get_field_id('col_played'); ?>"><?php _e('Played'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_win; ?> id="<?php echo $this->get_field_id('col_win'); ?>" name="<?php echo $this->get_field_name('col_win'); ?>" /> <label for="<?php echo $this->get_field_id('col_win'); ?>"><?php _e('Wins'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_loss; ?> id="<?php echo $this->get_field_id('col_loss'); ?>" name="<?php echo $this->get_field_name('col_loss'); ?>" /> <label for="<?php echo $this->get_field_id('col_loss'); ?>"><?php _e('Losses'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_draw; ?> id="<?php echo $this->get_field_id('col_draw'); ?>" name="<?php echo $this->get_field_name('col_draw'); ?>" /> <label for="<?php echo $this->get_field_id('col_draw'); ?>"><?php _e('Draws'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_forfeits; ?> id="<?php echo $this->get_field_id('col_forfeits'); ?>" name="<?php echo $this->get_field_name('col_forfeits'); ?>" /> <label for="<?php echo $this->get_field_id('col_forfeits'); ?>"><?php _e('Forfeits'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_points; ?> id="<?php echo $this->get_field_id('col_points'); ?>" name="<?php echo $this->get_field_name('col_points'); ?>" /> <label for="<?php echo $this->get_field_id('col_points'); ?>"><?php _e('Points'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_ptfor; ?> id="<?php echo $this->get_field_id('col_ptfor'); ?>" name="<?php echo $this->get_field_name('col_ptfor'); ?>" /> <label for="<?php echo $this->get_field_id('col_ptfor'); ?>"><?php _e('Points For'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_ptagst; ?> id="<?php echo $this->get_field_id('col_ptagst'); ?>" name="<?php echo $this->get_field_name('col_ptagst'); ?>" /> <label for="<?php echo $this->get_field_id('col_ptagst'); ?>"><?php _e('Points Against'); ?></label><br/>
			<input class="checkbox" type="checkbox" <?php echo $col_pct; ?> id="<?php echo $this->get_field_id('col_pct'); ?>" name="<?php echo $this->get_field_name('col_pct'); ?>" /> <label for="<?php echo $this->get_field_id('col_pct'); ?>"><?php _e('Percentage'); ?></label><br/>
		</p>
<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args((array) $new_instance, array( 'title' => '', 'competitionId' => '', 'col_image' => '', 'col_name' => '', 'col_played' => '', 'col_win' => '', 'col_loss' => '', 'col_draw' => '', 'col_forfeits' => '', 'col_points' => '', 'col_ptfor' => '', 'col_ptagst' => '', 'col_pct' => '' ));		
		
		$instance['title'] 			= strip_tags($new_instance['title']);
		$instance['competitionId']  = $new_instance['competitionId'];
		
		$instance['col_image'] 		= $new_instance['col_image'] ? 1 : 0;
		$instance['col_name'] 		= $new_instance['col_name'] ? 1 : 0;
		$instance['col_played'] 	= $new_instance['col_played'] ? 1 : 0;
		$instance['col_win'] 		= $new_instance['col_win'] ? 1 : 0;
		$instance['col_loss'] 		= $new_instance['col_loss'] ? 1 : 0;
		$instance['col_draw'] 		= $new_instance['col_draw'] ? 1 : 0;
		$instance['col_forfeits']	= $new_instance['col_forfeits'] ? 1 : 0;
		$instance['col_points'] 	= $new_instance['col_points'] ? 1 : 0;
		$instance['col_ptfor'] 		= $new_instance['col_ptfor'] ? 1 : 0;
		$instance['col_ptagst'] 	= $new_instance['col_ptagst'] ? 1 : 0;
		$instance['col_pct'] 		= $new_instance['col_pct'] ? 1 : 0;
		
		return $instance;
	}
}


add_action( 'widgets_init', 'fr_register_widget' );
function fr_register_widget() {
	register_widget('fr_ladder_widget');
}
?>